# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Utilities and scripts/projects in Google Cloud in support of our PC 4.0 Project
* Many tasks need to be executed across multiple projects in GCP, these tools should make this easier
* Version 0.1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* You can use the utilities
* Update the qad-project-* files with a list of your projects, you can insert a comment # to ignore certain projects
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact